#!/bin/bash
set -e
cat > .env <<EOF
VAULT_TOKEN=${VAULT_TOKEN}
VAULT_HOST=${VAULT_HOST}
VAULT_PORT=${VAULT_PORT}
VAULT_DEV_ROOT_TOKEN_ID=${VAULT_DEV_ROOT_TOKEN_ID}
EOF
if [ "$( docker container inspect -f '{{.State.Running}}' vault )" == "false" ]; then
docker rm vault || true 
docker run -d --cap-add=IPC_LOCK --restart always \
  --env-file .env  --name vault -p 8200:8200 \
  -e 'VAULT_SERVER=http://127.0.0.1:8200' -e 'VAULT_ADDR=http://127.0.0.1:8200' vault 
cat <<EOF | docker exec -i vault ash
  sleep 10;
  vault login ${VAULT_DEV_ROOT_TOKEN_ID}
  vault secrets enable -path=secret kv
  vault kv put secret/sausage-store spring.datasource.password="${SPRING_DATASOURCE_PASSWORD}" spring.data.mongodb.uri="${SPRING_DATA_MONGODB_URI}" spring.datasource.url="${SPRING_DATASOURCE_URL}" spring.datasource.username="${SPRING_DATASOURCE_USERNAME}"
  exit
EOF
fi
docker-compose stop backend-report || true
docker-compose rm -f backend-report || true
docker-compose pull backend-report
docker-compose create backend-report
docker-compose start backend-report
docker image prune -f
