#!/bin/bash
set -e
docker-compose stop frontend || true
docker-compose rm -f frontend || true
docker-compose pull frontend
docker-compose create frontend
docker-compose start frontend
docker image prune -f
